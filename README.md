## Clone Repository
```bash
git clone git@bitbucket.org:dobryy/blog.git
```

In case you don't want to use bitbucket account use:
```bash
git clone https://dobryy@bitbucket.org/dobryy/blog.git
```

## Run Container
Run Docker 
```bash
docker-compose up
```

Run user fixture to create to create administrator
```bash
docker exec -it blog_php-fpm_1 php bin/console doctrine:fixtures:load
```

## Administration
URL: http://localhost/admin/

Credentials: my-email@email.com/test

## Frontend
URL http://localhost